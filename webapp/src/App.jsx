import React, { Component } from 'react';
import { BarChart, Bar, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

const moment = require('moment');


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      readings: null
    };
  }

  componentDidMount() {
    fetch('/bulb/api/readings?detail=1')
      .then(response => response.json())
      .then(readings => this.setState({ readings }));
  }

  getRows(readings) {
    return readings.map(reading => (
      <tr key={reading.readingDate}>
        <td>
          {moment.utc(reading.readingDate).format('MMM')}<br/>
          {moment.utc(reading.readingDate).format("YYYY")}
        </td>
        <td>{reading.cumulative}</td>
        <td>{reading.interpolated || '-'}</td>
        <td>{reading.usage || '-'}</td>
        <td>{reading.unit}</td>
      </tr>
    ));
  }

  getEnergyUsageData(readings) {
    return readings.map(reading => ({
      'date': moment.utc(reading.readingDate).format("MMM 'YY"),
      'Usage': reading.usage || '-'
    }));
  }

  render() {
    const { readings } = this.state;
    if (!readings) {
      return null;
    }

    return (
      <div>
        <h1>Energy Usage</h1>
        <BarChart width={1400} height={400} data={this.getEnergyUsageData(readings)}>
          <XAxis dataKey="date" />
          <YAxis dataKey="Usage" />
          <CartesianGrid horizontal={false} />
          <Tooltip />
          <Bar dataKey="Usage" fill="#03ad54" isAnimationActive={false} />
        </BarChart>

        <h1>Meter Readings</h1>
        <table>
          <tbody>
            <tr>
              <th>Year</th>
              <th>Reading</th>
              <th>Interpolated</th>
              <th>Usage</th>
              <th>Unit</th>
            </tr>
            {this.getRows(readings)}
          </tbody>
        </table>
      </div>
    );
  }
};
