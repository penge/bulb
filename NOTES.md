Candidate Name: Pavel Bucka

Tasks: 1 + 2 + 3  (focus on 2 and 3)

Time: 1 + 2 + 2 (+1 to play around)

Notes:

It was fun to do so I did all tasks.
Task 2 and 3 are my focus though.


## Task 2 (API)

- adding methods to `data.js` (serves like DAO, API stays clean)
- promisifying `data.js` methods => using async/await in `index.js`
- GET `/readings` --- returns all readings
- GET `/readings?detail=1` --- returns all readings with "interpolated" and "usage"
- POST `/readings` --- adds a new reading, returns 400 on missing attribute(s)
- `koa-bodyparser` used to parse the request body


## Task 3 (Interpolation and Usage)

- adding own solution as `interpolate.js`
- using `dayOfYear()` to easily interpolate in `interpolateReading.js`
- adding `interpolateReadings.js` to add estimated end of month value to all readings
- adding `addReadingsUsage.js` to add usage per month to all readings


## Task 1 (WEB)

- fetching API data, only 1 API call made
- using `React.Component` and lifecycle methods like `componentDidMount()`
- adding rewrite rule to proxy settings in `webpack.config.js`
- adding some styles


**In case of any questions:**
- skype: pavelbucka
- phone: +420723006053
- email: bucka.pavel@gmail.com
