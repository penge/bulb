const { expect } = require('chai');
const interpolateReading = require('./interpolateReading');

describe('interpolateReading()', () => {
  it('returns reading with interpolated attribute', () => {
    const currentReading = {
      cumulative: 18002,
      readingDate: '2017-05-08T00:00:00.000Z',
      unit: 'kWh'
    };

    const nextReading = {
      cumulative: 18270,
      readingDate: '2017-06-18T00:00:00.000Z',
      unit: 'kWh'
    };

    const reading = interpolateReading(currentReading, nextReading);

    // Same as currentReading, "interpolated" added
    expect(reading.interpolated).to.equal(18025);
    expect(reading.cumulative).to.equal(18002);
    expect(reading.readingDate).to.equal('2017-05-08T00:00:00.000Z');
    expect(reading.unit).to.equal('kWh');
  })
})
