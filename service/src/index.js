const Koa = require('koa');
const KoaRouter = require('koa-router');
const data = require('./data');
const interpolateReadings = require('./interpolateReadings');
const addReadingsUsage = require('./addReadingsUsage');

const PORT = process.env.PORT || 3000;

function createServer() {
  const server = new Koa();
  const router = new KoaRouter();
  const bodyParser = require('koa-bodyparser');

  router.get('/', (ctx, next) => {
    ctx.body = 'Hello world';
  });

  router.get('/readings', async (ctx, next) => {
    const allReadings = await data.getAllReadings();
    if (1 == ctx.request.query.detail) {
      interpolateReadings(allReadings);
      addReadingsUsage(allReadings);
    };
    ctx.body = allReadings;
  });

  router.post('/readings', async (ctx, next) => {
    const oneReading = ctx.request.body;
    if (!oneReading.cumulative || !oneReading.readingDate || !oneReading.unit) {
      ctx.response.status = 400; // Bad request
      return;
    };

    ctx.body = await data.addReading(oneReading);
  });

  server.use(bodyParser());
  server.use(router.allowedMethods());
  server.use(router.routes());

  return server;
}

module.exports = createServer;

if (!module.parent) {
  data.initialize();
  const server = createServer();
  server.listen(PORT, () => {
    console.log(`server listening on port ${PORT}`);
  });
}
