const { expect } = require('chai');
const interpolate = require('./interpolate');

describe('interpolate()', () => {
  it('returns interpolated value', () => {
    const x1 = 128;
    const y1 = 18002;

    const x2 = 169;
    const y2 = 18270;

    const x = 151;
    const y = interpolate(x1, y1, x2, y2, x);

    expect(y).to.equal(18025);
  })
})
