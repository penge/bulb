module.exports = readings => {
  for (let index = 1; index < readings.length; index++) {
    readings[index].usage =
      (readings[index].interpolated || readings[index].cumulative) -
      (readings[index - 1].interpolated || readings[index - 1].cumulative);
  }
}
