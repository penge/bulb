const sqlite3 = require('sqlite3').verbose();
const sampleData = require('../sampleData.json');

const connection = new sqlite3.Database(':memory:');

/**
 * Imports the data from the sampleData.json file into a `meter_reads` table.
 * The table contains three columns - cumulative, reading_date and unit.
 *
 * An example query to get all meter reads,
 *   connection.all('SELECT * FROM meter_reads', (error, data) => console.log(data));
 *
 * Note, it is an in-memory database, so the data will be reset when the
 * server restarts.
 */
function initialize() {
  connection.serialize(() => {
    connection.run('CREATE TABLE meter_reads (cumulative INTEGER, readingDate TEXT, unit TEXT)');

    const { electricity } = sampleData;
    electricity.forEach((data) => {
      connection.run(
        'INSERT INTO meter_reads (cumulative, readingDate, unit) VALUES (?, ?, ?)',
        [data.cumulative, data.readingDate, data.unit],
      );
    });
  });
}

const getAllReadings = () => {
  return new Promise((resolve, reject) => {
    connection.all('SELECT * FROM meter_reads', (error, data) => {
      if (error) { return reject(error) }

      resolve(data)
    })
  })
}

const addReading = data => {
  return new Promise((resolve, reject) => {
    connection.run(
      'INSERT INTO meter_reads (cumulative, readingDate, unit) VALUES (?, ?, ?)',
      [data.cumulative, data.readingDate, data.unit], (result, error) => {
        if (error) { return reject(error) }

        resolve(data)
      }
    )
  })
}

module.exports = {
  initialize,
  connection,
  getAllReadings,
  addReading
}
