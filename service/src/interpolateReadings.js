const interpolateReading = require('./interpolateReading');


module.exports = readings => {
  for (let index = 0; index < readings.length - 1; index++) {
    readings[index] = interpolateReading(
      readings[index],
      readings[index + 1]
    )
  }
}
