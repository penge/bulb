const { expect } = require('chai');

const data = require('./data');
const sampleData = require('../sampleData.json');
const sampleDataLength = sampleData.electricity.length;

describe('data', () => {
  describe('getAllReadings()', () => {
    it('imports data from file', (done) => {
      data.initialize();

      data.connection.serialize(() => {
        data.connection.all('SELECT * FROM meter_reads ORDER BY cumulative', (error, selectResult) => {
          expect(error).to.be.null;
          expect(selectResult).to.have.length(sampleDataLength);
          expect(selectResult).to.deep.equal(sampleData.electricity);
          done();
        })
      })
    })
  })

  describe('getAllReadings()', () => {
    it('returns all readings', async () => {
      const readings = await data.getAllReadings();

      expect(readings).to.have.length(sampleDataLength);
    })
  })

  describe('addReading()', () => {
    it('adds new reading', async () => {
      const reading = {
        cumulative: 20750,
        readingDate: '2018-05-27T00:00:00.000Z',
        unit: 'kWh'
      };

      await data.addReading(reading);

      const allReadings = await data.getAllReadings();
      expect(allReadings).to.have.length(sampleDataLength + 1);
      expect(allReadings[sampleDataLength]).to.deep.equal(reading);
    })
  })
})
