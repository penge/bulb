/**
 * Interpolates unknown y for known x
 * @param {Number} x1
 * @param {Number} y1
 *
 * @param {Number} x2
 * @param {Number} y2
 *
 * @param {Number} x
 * @returns {Number} y
 */
module.exports = (
  x1, y1,
  x2, y2,
  x
) => {
  const y = y1 + (((x - x1) / (x2 - x1)) * (x2 - x1));

  return y;
}
