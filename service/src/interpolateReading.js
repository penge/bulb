const interpolate = require('./interpolate');
const moment = require('moment');

module.exports = (currentReading, nextReading) => {
  // Current month
  const x1 = moment.utc(currentReading.readingDate).dayOfYear();
  const y1 = currentReading.cumulative;

  // Next month
  const x2 = moment.utc(nextReading.readingDate).dayOfYear();
  const y2 = nextReading.cumulative;

  // End of current month
  const x = moment.utc(nextReading.readingDate).date(0).dayOfYear();
  const y = interpolate(x1, y1, x2, y2, x);


  // Return interpolated
  return Object.assign({ interpolated: y }, currentReading);
}
